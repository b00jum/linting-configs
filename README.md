# JS / TS / CSS Linting Configs

Personal configs and rules for [`prettier`](https://prettier.io/), [`stylelint`](https://stylelint.io/), and [`eslint`](https://eslint.org/).

|                                                                           |            |
| :------------------------------------------------------------------------ | :--------- |
| [@boojum/eslint-config-js](./packages/eslint-config-js/README.md)         | JavaScript |
| [@boojum/prettier-config-base](./packages/prettier-config-base/README.md) | Formatting |

## Versioning and Publishing

Version bumps are performed manually with `npm version`, for example `npm -w packages/prettier-config-base version patch`.

Publishing happens automatically via GitLab CI if changes to the package are detected.
