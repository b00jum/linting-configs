# `@boojum/ts-config-base`

An opinionated [TypeScript](https://www.typescriptlang.org/) shareable config. It's designed to help you maintain a consistent code style and quality in your projects.

## Installation

You can install the configuration and its dependencies using npm:

```sh
npm i -D @boojum/ts-config-base
```

## Usage

After installing the package, you need to extend configuration in your `tsconfig.json` file:

```json
{
  "extends": "@boojum/ts-config-base"
}
```
