# `@boojum/prettier-config-base`

An opinionated [Prettier](https://prettier.io/) shareable config. It's designed to help you maintain a consistent code style and quality in your projects.

**NOTE:** Make sure that you have Prettier (version `3.0.0` or higher) installed in your project since this configuration relies on the ESM (ECMAScript Modules) syntax for the config file.

## Installation

You can install the configuration and its dependencies using npm:

```sh
npm i -D @boojum/prettier-config-base
```

## Usage

After installing the package, you need to extend the configuration in your Prettier configuration file. Here's an example of how to do it in a `prettier.config.js` file:

```js
import prettierBaseConfig from '@boojum/prettier-config-base'

export default {
  ...prettierBaseConfig,
  // Custom rules or overrides can be added here
}
```
