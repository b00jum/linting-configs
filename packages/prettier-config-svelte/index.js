import prettierConfigBase from '@boojum/prettier-config-base'

export default {
  ...prettierConfigBase,
  plugins: ['prettier-plugin-svelte'],
  overrides: [{ files: '*.svelte', options: { parser: 'svelte' } }],
}
