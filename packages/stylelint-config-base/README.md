# `@b00jum/stylelint-config-base`

An opinionated [Stylelint](https://stylelint.io/) shareable config. It's designed to help you maintain a consistent code style and quality in your projects.

## Installation

You can install the configuration and its dependencies using npm:

```sh
npm i -D @b00jum/stylelint-config-base
```

## Usage

After installing the package, you need to extend the configuration in your Stylelint configuration file. Here's an example of how to do it in a `stylelint.config.cjs` file:

```js
module.exports = {
  extends: '@b00jum/stylelint-config-base',
  // Other custom rules or overrides can be added here.
}
```
