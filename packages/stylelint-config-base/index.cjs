module.exports = {
  extends: ['stylelint-config-standard'],
  overrides: [
    {
      files: ['**/*.html'],
      extends: ['stylelint-config-html/html'],
    },
  ],
}
