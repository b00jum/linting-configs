import eslintConfigTs from '@boojum/eslint-config-ts'
import eslintConfigJsNode from '@boojum/eslint-config-js-node'

export default [...eslintConfigTs, ...eslintConfigJsNode]
