import eslintBaseConfig from '@boojum/eslint-config-js'
import globals from 'globals'
import nodePlugin from 'eslint-plugin-n'

export default [
  ...eslintBaseConfig,
  {
    languageOptions: {
      globals: {
        ...globals.nodeBuiltin,
      },
    },
    plugins: {
      n: nodePlugin,
    },
    rules: {
      ...nodePlugin.configs['flat/recommended-module'].rules,
    },
  },
]
