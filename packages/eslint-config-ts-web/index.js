import eslintConfigWeb from '@boojum/eslint-config-js-web'
import eslintConfigTs from '@boojum/eslint-config-ts'

export default [...eslintConfigWeb, ...eslintConfigTs]
