import eslintBaseConfig from '@boojum/eslint-config-js'
import globals from 'globals'

export default [
  ...eslintBaseConfig,
  {
    languageOptions: {
      globals: {
        ...globals.browser,
      },
    },
  },
]
