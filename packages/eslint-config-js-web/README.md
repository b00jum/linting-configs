# `@boojum/eslint-config-js-web`

An opinionated [ESLint](https://eslint.org/) shareable config. It's designed to help you maintain a consistent code style and quality in your JavaScript projects.

**NOTE:** Make sure that you have ESLint (version `8.0.0` or higher) installed in your project since this configuration relies on the ESM (ECMAScript Modules) syntax for the config file, often referred to as a flat config.

## Installation

You can install the configuration and its dependencies using npm:

```sh
npm i -D @boojum/eslint-config-js-web
```

## Usage

After installing the package, you need to extend the configuration in your ESLint configuration file. Here's an example of how to do it in a `eslint.config.js` file:

```js
import eslintConfigWeb from '@boojum/eslint-config-js-web'

export default [
  ...eslintConfigWeb,
  // Custom rules or overrides can be added here
]
```
