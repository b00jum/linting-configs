module.exports = {
  extends: ['@boojum/stylelint-config-base'],
  overrides: [
    {
      files: ['**/*.svelte'],
      extends: ['stylelint-config-html/svelte'],
      rules: {
        // stop complaining about unknown pseudo-class selector ":global"
        'selector-pseudo-class-no-unknown': [true, { ignorePseudoClasses: ['global'] }],
      },
    },
  ],
}
