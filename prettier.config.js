import prettierBaseConfig from './packages/prettier-config-base/index.js'

export default {
  ...prettierBaseConfig,
}
